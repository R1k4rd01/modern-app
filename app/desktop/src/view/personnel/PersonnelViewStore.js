const pokeGet = () => {
    axios
        .get("https://pokeapi.co/api/v2/pokemon/")
        .then(function (response) {
            // handle success
            Ext.define('ModernApp.view.personnel.PersonnelViewStore', {
                extend: 'Ext.data.Store',
                alias: 'store.personnelviewstore',
                fields: [
                    'name', 'email', 'phone', 'dept'
                ],
                groupField: 'dept',
                data: {
                    items: response.data.results
                },
                proxy: {
                    type: 'memory',
                    reader: {
                        type: 'json',
                        rootProperty: 'items'
                    }
                }
            });
        })
        .catch(function (error) {
            // handle error
            console.log(error);
        })
        .then(function () {
            // always executed
        });
};

pokeGet();

